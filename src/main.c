#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>

#include "headers/models.h"
#include "headers/constants.h"
#include "lib/vector/vector.h"
#include "lib/random.h"

int DEBUG_MODE = 0;

int client_count = 100;
//initialisations
int waiting_client_count=0;    //Nombre de clients en attente d'etre coiffes
int nbTicket = 500;// nombre de ticket diponible

Client **clients;
Movie **movies;
Room **rooms;
Seance **seances;

int room_count = 5;
int room_capacity = 10;
int seance_interval = 3;
int movie_count = 5;
int session_count = 3;
int seance_count;



pthread_t *clients_tid;
pthread_t *seances_tid;
pthread_mutex_t mutexInternet; //creation d'un mutex pour achat via internet
pthread_mutex_t mutexGuichet; //creation d'un mutex pour achat via guichet
pthread_mutex_t mutexBorne; //creation d'un mutex pour achat via borne
pthread_cond_t attendre, dormir; //point de signalisation




void * seances_stop(void *seance);
void * seance_start(void *seance);






/*
RESERVATIONS PROCESS:


			 CLIENTS
					|											|||||||||| [BORNE] (queuing required)
					v										 /
|||||||||||||||||||||||||||||||-|||||||||| [GUICHET] (queuing required)
															 \
																|||||||||| [INTERNET]



MOVIE SCHEDULING AND SESSION LIFECYCLE:

																	SESSION START INDEX
																				|
																				|			session ends here
																				|						|
																				V      			v
															|------Session 1------|-------Session 2------| etc.
															|---------------------+----------------------| ................
reservation process --------> |seance:m1 --->       |seance:m1 --->        | ................
reservation process --------> |seance:m2 ------>    |seance:m2 ------>     | ................
reservation process --------> |seance:m3 --->       |seance:m3 --->        | ................
reservation process --------> |seance:m4 -------->  |seance:m4 -------->   | ................
reservation process --------> |seance:m5 ----->     |seance:m5 ----->      | ................
															----------------------------------------------

End of seance:
-Room is emptied (places taken 0) and state full transitions to false
-Every client in the room is freed from memory

*/


/////////////////////// achat via internet
void internet ()
{

	pthread_mutex_lock(&mutexInternet); //verouille mutexInternet
	if(waiting_client_count>0)
	{
		// dit à un client de s'installer sur le fauteuil
		printf("le client se sert en ligne \n");
		pthread_cond_signal(&attendre); //reveil le point d'achat en ligne si nombre de client > 0
	}
	else       {
		pthread_cond_wait(&dormir, &mutexInternet); //le d'achat en ligne se met en attente sur la variable mutex : Libération du mutex + Blocage systématique de l’appelant de manière atomique
		printf("internet libre \n");

		pthread_cond_signal(&attendre); // permet à un client de faire son achat
	}
	pthread_mutex_unlock(&mutexInternet); //deverouille mutex

}

//service au guichet
void guichet ()
{

	pthread_mutex_lock(&mutexGuichet); //
	if(waiting_client_count>0)
	{
		pthread_cond_signal(&attendre);
		printf("Vend un ticket au client au guichet \n");
	}
	else{
		pthread_cond_wait(&dormir, &mutexGuichet);
		printf("guichet libre \n");
		pthread_cond_signal(&attendre);
	}
	pthread_mutex_unlock(&mutexGuichet);
}


void borne ()
{

	pthread_mutex_lock(&mutexBorne); //verouille mutex
	if(waiting_client_count>0)
	{
		pthread_cond_signal(&attendre);
	}
	else{
		pthread_cond_wait(&dormir, &mutexBorne);
		printf("borne libre\n");
		pthread_cond_signal(&attendre);
	}
	pthread_mutex_unlock(&mutexBorne);
}

int a = 0;
void clientTicket(int x)
{

	int r = rand()%(3)+1;//a[i]=rand()%(1000)+a entre [a,a+1000[;
	int s = rand()%(15);

	if(r == 1){
		pthread_mutex_lock(&mutexInternet); //verouille mutexInternet selon le resultat du random
		if(waiting_client_count<nbTicket)
		{
			// s'installe pour l'achat dans la file
			pthread_cond_signal(&dormir); //
			waiting_client_count ++;//incremente le nombre de client
			printf("Le client %d se met dans la file\n",x);
			// on deverouille le mutex et attend que la condition soit signalée
			// attend son tour d'achat
			pthread_cond_wait(&attendre, &mutexInternet);// c'est son tour
			waiting_client_count --;//decremente le nombre de client
			printf("Le client %d est servi sur internet\n",(int)x);
			int n = 0;

			while((n < room_capacity) && (seances[s]->clients[n] != NULL)){
				n++;
			}
			if(seances[s]->room->full){
				if(seances[s]->state == SEANCE_STATE_IDLE)
					pthread_create(&seances_tid[s], NULL, seance_start, (void*)seances[s]);




				printf("la seance %p est complete !!! Les ventes sont fermées.\n", seances[s]);
				printf("le client %d sort du cinema\n", x);
			}
			else{
				//creation d'un ticket pour une seance
				Ticket *ticket;
				ticket = (Ticket*)malloc(sizeof(Ticket));
				ticket->seance = seances[s];
				ticket->ticket_type = TICKET_TYPE_MACHINE;
				//creation du client
				Client *client;
				client = (Client*)malloc(sizeof(Client));
				client->ticket = ticket;
				client->age = rand()%(90)+5;
				//ajout du client dans la liste de la seance

				clients[a] = client;
				seances[s]->clients[n] = client;

				if(room_take_place(seances[s]->room)){
					printf("Le client %d a pris sa place et attends le debut du film %p...\n", x, seances[s]->movie);
				}



				//creer le ticket correspondant
				nbTicket--;
				a++;

			}
		}
		else printf("Le client %d ne trouve pas de place\n", x);
		pthread_mutex_unlock(&mutexInternet);
	}

	if(r == 2){
		pthread_mutex_lock(&mutexGuichet);
		if(waiting_client_count<nbTicket)
		{
			pthread_cond_signal(&dormir);
			waiting_client_count ++;
			printf("Le client %d se met dans la file\n",x);

			// on deverouille le mutex et attend que la condition soit signalée
			// attend qu'on lui dise de s'installer
			pthread_cond_wait(&attendre, &mutexGuichet);
			waiting_client_count --;
			printf("Le client %d est servi au guichet \n",(int)x);
			int n = 0;

			while((n < room_capacity) && (seances[s]->clients[n] != NULL)){
				n++;
			}
			if(seances[s]->room->full){


				if(seances[s]->state == SEANCE_STATE_IDLE)
					pthread_create(&seances_tid[s], NULL, seance_start, (void*)seances[s]);


				printf("la seance %p est complete!!! Les ventes sont fermées.\n", seances[s]);
				printf("le client %d sort du cinema\n", x);
			}
			else{
				//creation d'un ticket pour une seance
				Ticket *ticket;
				ticket = (Ticket*)malloc(sizeof(Ticket));
				ticket->seance = seances[s];
				ticket->ticket_type = TICKET_TYPE_STANDARD;
				//creation du client
				Client *client;
				client = (Client*)malloc(sizeof(Client));
				client->ticket = ticket;
				client->age = rand()%(90)+5;
				//ajout du client dans la liste de la seance
				clients[a] = client;
				seances[s]->clients[n] = client;
				if(room_take_place(seances[s]->room)){
					printf("Le client %d a pris sa place et attends le debut du film %p...\n", x, seances[s]->movie);
				}
				//creer le ticket correspondant
				nbTicket--;
				a++;

			}
		}
		else printf("Le client %d ne trouve pas de place\n", x);
		pthread_mutex_unlock(&mutexGuichet);
	}

	if(r == 3){
		pthread_mutex_lock(&mutexBorne);
		if(waiting_client_count<nbTicket)
		{
			pthread_cond_signal(&dormir);
			waiting_client_count ++;

			printf("Le client %d se met dans la file\n",x);

			// on deverouille le mutex et attend que la condition soit signalée
			// attend qu'on lui dise de s'installer
			pthread_cond_wait(&attendre, &mutexBorne);
			waiting_client_count --;
			printf("Le client %d se sert a la borne \n",(int)x);

			int n = 0;

			while((n < room_capacity) && (seances[s]->clients[n] != NULL)){
				n++;
			}
			if(seances[s]->room->full){
				if(seances[s]->state == SEANCE_STATE_IDLE)
					pthread_create(&seances_tid[s], NULL, seance_start, (void*)seances[s]);

				printf("la seance %p est complete!!! Les ventes sont fermées.\n", seances[s]);
				printf("le client %d sort du cinema\n", x);
			}
			else{
				//creation d'un ticket pour une seance
				Ticket *ticket;
				ticket = (Ticket*)malloc(sizeof(Ticket));
				ticket->seance = seances[s];
				ticket->ticket_type = TICKET_TYPE_MACHINE;
				//creation du client
				Client *client;
				client = (Client*)malloc(sizeof(Client));
				client->ticket = ticket;
				client->age = rand()%(90)+5;
				//ajout du client dans la liste de la seance

				clients[a] = client;
				seances[s]->clients[n] = client;
				if(room_take_place(seances[s]->room)){
					printf("Le client %d a pris sa place et attends le debut du film %p...\n", x, seances[s]->movie);
				}
				//creer le ticket correspondant
				nbTicket--;
				a++;
			}

		}
		else printf("Le client %d ne trouve pas de place\n", x);
		pthread_mutex_unlock(&mutexBorne); //deverouille mutex
	}

}

/////////////////////// traitant achat en ligne
void * foncInternet()
{

	while (1)  {
		internet();
		/* temps de coiffure d'un client */
		usleep(20000);
	}
}
/////////////////////// traitant guichet
void * foncGuichet()
{

	while (1)  {
		guichet();
		/* temps de coiffure d'un client */
		usleep(20000);
	}
}
/////////////////////// traitant borne
void * foncBorne()
{

	while (1)  {
		borne();
		/* temps d'achat */
		usleep(20000);
	}
}

/////////////////////// traitant client
void * foncClientTicket(void *i)
{

	int * num = (int*)i;
	clientTicket(*num);
	usleep(20000);
	return NULL;
}












/////////////////////////////////////////////////////////////////////////////////////////////////////////


void * seances_stop(void *seance){
	Seance *s = (Seance*)seance;
	printf("SEANCE %p ENDED.\n", s );
	int i;
	for(i = 0; i < room_capacity; i++){
		//free(s->clients[i]);
		printf("Client is leaving the session...\n");
		s->clients[i] = NULL;
		room_leave_place(s->room);
	}


	s->room->full = 0;
	s->state = SEANCE_STATE_IDLE;
	return NULL;

}



void * seance_start(void *seance){

	Seance *s = (Seance*)seance;
	printf("SEANCE %p STARTED, duration %f...\n", s, s->movie->duration);
	s->state = SEANCE_STATE_STARTED;

	sleep((int)s->movie->duration);


	seances_stop((void*)s);

	return NULL;

}




/////////////////////////////////////////////////////////////////////////////////////////////////////



void free_and_quit(){
	printf("-----FREEING ALL DYNAMICALLY ALOCATED MEMORY...\n" );


	int i;

	for(i=0;i<client_count;i ++)
		pthread_cancel(*clients_tid+i);

	for(i = 0; i < seance_count; i++)
		pthread_cancel(seances_tid[i]);


	free(clients_tid);
	free(seances_tid);
	free(clients);
	free(seances);
	free(rooms);
	free(movies);
	printf("MEMORY FREED!\nCLOSING THE SIMULATION GRACEFULLY...\n");
	exit(0);
}





/*
###############################################################################################################
###############################################################################################################
###############################################################################################################
#################    MAIN 							              MAIN 								                    #################
#################    MAIN MAIN 					         MAIN MAIN								                    #################
#################	   MAIN MAIN MAIN 		    MAIN MAIN MAIN								                    #################
#################	   MAIN 	 MAIN MAIN MAIN MAIN 	    MAIN								                    #################
#################	   MAIN 		  MAIN MAIN 		        MAIN								                    #################
#################	   MAIN 		  	MAIN 	 		          MAIN								                    #################
#################	   MAIN 							              MAIN								                    #################
#################																			                                        #################
###############################################################################################################
###############################################################################################################
###############################################################################################################
*/


int main(int argc, char* argv[]){
	srand(time(NULL));

	printf("Movie Theater Simulation Version: %s\n", VER);
	printf("-----------------------------------------------------\n\n");

	signal(SIGINT, free_and_quit);

	///////////////////////////////INIT/////////////////////////////////////

	printf("Defining variables...\n");

	printf("Automatic setup mode? (0/1, default 1): ");
	scanf("%d", &DEBUG_MODE);


	if(DEBUG_MODE == 0){
		printf("Seance count: ");
		scanf("%d", &seance_count);

		printf("Room count: ");
		scanf("%d", &room_count);

		printf("Client count: ");
		scanf("%d", &client_count);
	}


	seance_count 	= 	movie_count * session_count;
	clients_tid 	= 	malloc(sizeof(pthread_t)*(client_count+1));
	seances_tid 	= 	malloc(sizeof(pthread_t)*session_count);
	clients 			= 	malloc(sizeof(Client)*client_count);
	seances 			= 	malloc(sizeof(Seance)*seance_count);
	rooms 				= 	malloc(sizeof(Room)*room_count);
	movies				=		malloc(sizeof(Movie)*movie_count);

	printf("Clients to generate: %d\n", client_count);

	//////////////////////MASS ALLOCATION////////////////////////////////////

	printf("Allocating memory...\n");

	int i;


	//ROOMS---------------
	for(i = 0; i < room_count; i++){

		Room *room;
		////TODO: dynamic memory allocation! DO NOT FORGET TO FREE THE MEM!
		room = (Room*)malloc(sizeof(Room));
		rooms[i] = room;
	}


	//MOVIES--------------
	for(i=0; i < movie_count; i++){

		Movie *movie;

		//TODO: dynamic memory allocation! DO NOT FORGET TO FREE THE MEM!
		movie = (Movie*)malloc(sizeof(Movie));
		movies[i] = movie;
	}


	//SEANCES-----------
	for(i = 0 ; i < seance_count ; i++){
		Seance *seance;
		//TODO: dynamic memory allocation! DO NOT FORGET TO FREE THE MEM!
		seance  = (Seance*)malloc(sizeof(Seance));
		seances[i] = seance;
	}


	/////////////// INIT PARAMS /////////////////////////////////////////


	printf("Initialising parameters for... \n");

	printf("\t...rooms\n" );


	for(i = 0; i < room_count; i++){
		rooms[i]->places_taken = 0;
		rooms[i]->capacity = room_capacity;
		rooms[i]->full = 0;
	}

	printf("\t...movies\n" );

	for(i = 0; i< movie_count; i++){

		char title[20];
		sprintf(title, "Movie %d", i);
		movie_set_title(movies[i], title);

		//movie->genre = &gen1;
		movies[i]->duration = random_to(seance_interval);;
	}

	printf("\t...seances\n" );

	//Sessions/Seances Creation

	printf("\t\tAssigning seances for today...\n");

	int session_index;
	for(session_index = 1; session_index <= session_count; session_index++ ){

		for (i = seance_count/session_count*(session_index-1); i < seance_count/session_count*session_index; i++){

			int movie_index;
			int room_index;

			movie_index = random_to(movie_count-1);
			room_index = random_to(room_count-1);

			seances[i]->movie       = movies[movie_index];
			seances[i]->room        = rooms[room_index];
			seances[i]->state       = SEANCE_STATE_IDLE;
			seances[i]->start_time  = session_index;
			seances[i]->clients = malloc(sizeof(Client)*room_capacity);

		}

	}

	for(i = 0; i< seance_count; i++) seance_display(seances[i]);

	///////////////achat ticket

	int num;
	//pthread_mutex_init(&mutex, NULL);

	pthread_mutex_init(&mutexInternet, NULL);
	pthread_mutex_init(&mutexGuichet, NULL);
	pthread_mutex_init(&mutexBorne, NULL);
	pthread_cond_init(&attendre, NULL);
	pthread_cond_init(&dormir, NULL);

	//
	//
	while(1){

		pthread_create(clients_tid+client_count, 0, foncInternet,	NULL);


		pthread_create(clients_tid+client_count, 0, foncGuichet,	NULL);


		pthread_create(clients_tid+client_count, 0, foncBorne,	NULL);

		//creation des threads clients
		for(num=0;num<client_count;num ++)
			pthread_create(clients_tid+num,0,(void *)foncClientTicket,(void*)&num);
		//
		//




		for(num=0;num<client_count;num ++)
			pthread_join(clients_tid[num],NULL);

		for(i = 0; i < seance_count; i++)
			pthread_join(seances_tid[i], NULL );


			printf("==========================================END OF THE DAY=====================================\n" );
	}


	/* liberation des ressources");*/





	return 0;

}
