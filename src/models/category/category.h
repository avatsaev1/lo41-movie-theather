#ifndef CATEGORY_H
#define CATEGORY_H


typedef enum Category{
  PG8, PG13, PG18
}Category;


char* category_to_string(Category* cat);


#endif
