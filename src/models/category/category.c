#include "category.h"
#include "../movie/movie.h"
#include <stdio.h>


char* category_to_string(Category* cat){
  char *categories[] = {"PG8", "PG13", "PG18"};

  return categories[(int)cat];
}
