#ifndef CLIENT_H
#define CLIENT_H


typedef struct Ticket Ticket;

#define CLIENT_STATE_WAITING_IN_LINE 0
#define CLIENT_STATE_BUYING_TICKET 1
#define CLIENT_STATE_SEATED 2
#define CLIENT_STATE_WATCHING 3
#define CLIENT_STATE_FINISHED_WATCHING 4


typedef struct Client{
  Ticket *ticket;
  int age;
}Client;


void client_display(Client *cli);

#endif
