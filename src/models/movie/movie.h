#ifndef MOVIE_H
#define MOVIE_H


typedef struct Movie{
  float duration;
  char title[20];
}Movie;


void movie_display(Movie *mov);

void movie_set_title(Movie *mov, char title[]);


#endif
