#include "movie.h"
#include "../genre/genre.h"
#include "../category/category.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void movie_display(Movie *mov){

  printf("---------MOVIE---------\n");
  printf("- title: %s\n", mov->title);
  //printf("- start time: %d\n", mov->start_time);
  printf("- duration: %f\n", mov->duration);
  //printf("- genre: %s\n", mov->genre->label);
  //printf("- category: %s\n", category_to_string(mov->category));


  // if(mov->room != NULL)
  //   printf("- room capacity: %d", mov->room->capacity);

  printf("\n");

}



void movie_set_title(Movie* m, char title[]){

  strcpy(m->title, title);
}
