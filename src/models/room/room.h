#ifndef ROOM_H
#define ROOM_H

typedef struct Room{
  int capacity;
  int places_taken; //critical ressource
  int full;
}Room;


void room_display(Room *room);
int room_take_place(Room *room);
void room_leave_place(Room *room);

#endif
