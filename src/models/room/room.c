#include "room.h"
#include <stdio.h>

void room_display(Room *room){
  printf("room capacity: %d\n", room->capacity);
}


int room_take_place(Room *room){

  if(room->places_taken < room->capacity){

    room->places_taken += 1;

    if(room->places_taken == room->capacity)
      room->full = 1;

		return 1;
  }
	return 0;
}

void room_leave_place(Room *room){

  if(room->places_taken > 0){

    room->places_taken -= 1;

    if(room->places_taken < room->capacity)
      room->full = 0;

  }

}
