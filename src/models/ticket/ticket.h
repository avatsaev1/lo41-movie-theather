#ifndef TICKET_H
#define TICKET_H


typedef struct Seance Seance;


#define TICKET_TYPE_ONLINE 0
#define TICKET_TYPE_MACHINE 1
#define TICKET_TYPE_STANDARD 2

typedef struct Ticket{
  int ticket_type;
  Seance *seance;
}Ticket;


void ticket_display(Ticket *tic);


#endif
