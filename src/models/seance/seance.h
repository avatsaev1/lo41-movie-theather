#ifndef SEANCE_H
#define SEANCE_H



typedef struct Movie Movie;
typedef struct Room Room;
typedef struct Client Client;

#define SEANCE_STATE_IDLE 0
#define SEANCE_STATE_STARTED 1
#define SEANCE_STATE_FINISHED 2

typedef struct Seance{
  Room *room;
	Movie *movie;
  int state;
  int start_time;
	Client **clients;
}Seance;


void seance_display(Seance*);


#endif
