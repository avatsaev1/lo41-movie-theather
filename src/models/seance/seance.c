#include <stdlib.h>
#include <stdio.h>
#include "seance.h"
#include "../../lib/vector/vector.h"


Vector* get_sessions_with_start_index(Seance* seances[], int index, int seance_count){

  //TODO: dynamic memory allocation! DO NOT FORGET TO FREE THE MEM!
  Vector *seances_to_return = (Vector*)malloc(sizeof(Vector));
  vector_init(seances_to_return);

  int i;
  for(i = 0; i < seance_count; i++){
    if(seances[i]->start_time == index){
      vector_append(seances_to_return, (int)(seances[i]));
    }
  }



  const int return_array_size = seances_to_return->size;
  Seance seances_[return_array_size];

  for(i = 0; i < return_array_size; i++){
    //seances_[i] = (Seance*)(*vector_get(seances_to_return,i));
  }



  return seances_to_return;

}


void seance_display(Seance* s){

  printf("------------SEANCE %p-----------\n",s );

  printf("Room: %p\n", s->room);
  printf("Movie: %p\n", s->movie);
  printf("State: %d\n", s->state);
  printf("Start Time: %d\n", s->start_time);

  printf("-------------------------------------\n");

}
