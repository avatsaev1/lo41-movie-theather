
#Movie Theater Simulator (LO41)

> **By Vatsaev Aslan and El Canah Zossou**

> UTBM 2015-2016

##Usage

###To compile in debug mode:


```
    make debug
```

or

```
    make debug
```


###To compile in release mode:


```
    make release
```



###To clean up:


```
    make clean
```


###To run in debug mode:


```
    make run_d
```

or


```
    make run
```


###To run in release mode:


```
    make run_r
```
