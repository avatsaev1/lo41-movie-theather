CC = gcc
LIBS = -lpthread

src := $(shell find . -name '*.c')
dbg_files := $(shell find . -name '*.h.gch')

bin_d := bin/debug
bin_r := bin/release
exec  := theather_sim

.PHONY: debug

debug: CCFLAGS += -DDEBUG -g -O0
debug:
	$(CC) $(CCFLAGS) $(LIBS) $(src) -o $(bin_d)/$(exec)

.PHONY: release

release: CCFLAGS += -Ofast
release:
	$(CC) $(CCFLAGS) $(LIBS) $(src) -o $(bin_r)/$(exec)

all:
	$(CC) $(CCFLAGS) $(LIBS) $(src) -o $(bin_d)/$(exec)

clean:

	rm -rf $(bin_d)/* $(bin_r)/*
	rm $(dbg_files)

run_d:
	$(bin_d)/$(exec)

run_r:
	$(bin_r)/$(exec)

run:
	$(bin_d)/$(exec)
